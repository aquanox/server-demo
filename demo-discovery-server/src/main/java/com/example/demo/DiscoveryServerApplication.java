package com.example.demo;

import org.springframework.boot.Banner.Mode;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author Aquanox
 * @date 20.09.2017 13:41
 */
@SpringBootApplication
@EnableEurekaServer
public class DiscoveryServerApplication
{
    public static void main(String[] args)
    {
        new SpringApplicationBuilder(DiscoveryServerApplication.class)
            .bannerMode(Mode.OFF)
            .run(args);
    }
}
