package com.demo.model;

import java.util.Date;

/**
 * @author Aquanox
 * @date 25.09.2017 13:16
 */
@lombok.Data
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
public class Email
{
    private String to;
    private String body;
    private Date createdAt;
}
