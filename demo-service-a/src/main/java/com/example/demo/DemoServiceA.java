package com.example.demo;

import com.example.AbstractService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class DemoServiceA extends AbstractService
{
    public static void main(String[] args)
    {
        runService(SpringApplication.run(DemoServiceA.class, args));
    }
}

