package com.demo;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ServerChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.net.InetSocketAddress;

/**
 * @author Aquanox
 * @date 20.09.2017 15:22
 */
@Service("nettyServer")
@Slf4j
public class NettyServer
{
    private ServerBootstrap bootstrap;
    private InetSocketAddress tcpPort;

    private ServerChannel serverChannel;

    @Autowired
    public NettyServer(@Qualifier("serverBootstrap") ServerBootstrap bootstrap,
                       @Qualifier("tcpSocketAddress") InetSocketAddress tcpPort)
    {
        this.bootstrap = bootstrap;
        this.tcpPort = tcpPort;
    }

    @PostConstruct
    public void startUp() throws InterruptedException
    {
        log.info("Starting server at port {}", tcpPort);

        serverChannel = (ServerChannel) bootstrap.bind(tcpPort).sync().channel();
    }

    @PreDestroy
    public void shutDown() throws InterruptedException
    {
        log.info("Closing server socket...");

        serverChannel.close().sync().await();
    }
}
