package com.demo;

import com.demo.samples.bus.BusConfig;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.jms.annotation.EnableJms;

/**
 * @author Aquanox
 * @date 20.09.2017 14:50
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableJms
@Import(BusConfig.class)
public class ArbiterServerApplication
{
    private static ConfigurableApplicationContext applicationContext;

    public static ConfigurableApplicationContext getApplicationContext()
    {
        return applicationContext;
    }


    public static void main(String[] args) throws InterruptedException
    {
        applicationContext = new SpringApplicationBuilder(ArbiterServerApplication.class).run(args);
        applicationContext.getBean(NettyServer.class);
    }
}
