package com.demo;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LoggingHandler;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;

import java.net.InetSocketAddress;

/**
 * @author Aquanox
 * @date 20.09.2017 15:07
 */
@Configuration
@Data
public class NettyServerConfig
{
    @Value("${netty.tcp.port}")
    private int tcpPort;

    @Value("${netty.core.boss-threads}")
    private int bossThreadCount;

    @Value("${netty.core.worker-threads}")
    private int workerThreadCount;

    @Value("${netty.core.log}")
    private boolean enableLogger = false;

    @Value("${netty.so.keep-alive}")
    private boolean keepAlive;

    @Value("${netty.so.backlog}")
    private int backlog;

    @Bean
    public NettyServerChannelInitializer channelInitializer() {
        return new NettyServerChannelInitializer();
    }

    @Bean(name = "bossGroup", destroyMethod = "shutdownGracefully")
    public NioEventLoopGroup bossGroup() {
        Assert.isTrue(bossThreadCount > 0, "Must specify at least one boss thread");
        return new NioEventLoopGroup(bossThreadCount);
    }

    @Bean(name = "workerGroup", destroyMethod = "shutdownGracefully")
    public NioEventLoopGroup workerGroup() {
        Assert.isTrue(workerThreadCount > 0, "Must specify at least one worker thread");
        return new NioEventLoopGroup(workerThreadCount);
    }

    @Bean(name = "tcpSocketAddress")
    public InetSocketAddress tcpPort() {
        return new InetSocketAddress(tcpPort);
    }

    @SuppressWarnings("unchecked")
    @Bean(name = "serverBootstrap")
    public ServerBootstrap bootstrap() {
        ServerBootstrap b = new ServerBootstrap();
        b.group(bossGroup(), workerGroup())
            .channel(NioServerSocketChannel.class)
            .option(ChannelOption.SO_KEEPALIVE, keepAlive)
            .option(ChannelOption.SO_BACKLOG, backlog)
            .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10000)
            .childHandler(channelInitializer());

        if (enableLogger) {
            b.handler(new LoggingHandler());
        }

        return b;
    }

}
