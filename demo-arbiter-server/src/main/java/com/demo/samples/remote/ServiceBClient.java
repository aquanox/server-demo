package com.demo.samples.remote;

import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * @author Aquanox
 * @date 20.09.2017 15:33
 */
@RibbonClient(name = "service-b", configuration = ServiceBClientConfig.class)
public class ServiceBClient
{


}
