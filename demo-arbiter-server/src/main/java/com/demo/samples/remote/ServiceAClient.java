package com.demo.samples.remote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.web.client.RestTemplate;

/**
 * @author Aquanox
 * @date 20.09.2017 15:33
 */
@RibbonClient(name = "service-a", configuration = ServiceAClientConfig.class)
public class ServiceAClient
{
    @Autowired @Qualifier("service-a-client")
    private RestTemplate restTemplate;

    public void pingMe()
    {
        String response = restTemplate.getForObject("http://service-a/", String.class);
        System.out.println(response);
    }
}
