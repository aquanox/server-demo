package com.demo.samples.remote;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author Aquanox
 * @date 20.09.2017 15:51
 */
@Configuration
public class ServiceAClientConfig extends ClientConfig
{
    @LoadBalanced
    @Bean(name = "service-a-client")
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
