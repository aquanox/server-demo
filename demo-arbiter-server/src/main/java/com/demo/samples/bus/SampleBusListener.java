package com.demo.samples.bus;

import com.demo.model.Email;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

/**
 * @author Aquanox
 * @date 30.09.2017 15:42
 */
@Service
public class SampleBusListener
{

    @JmsListener(destination = "mailbox")
    public void receiveMessage(Email email)
    {
        System.out.println(System.nanoTime() + "| Received <" + email + ">");
    }
}
