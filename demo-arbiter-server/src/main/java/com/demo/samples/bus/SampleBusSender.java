package com.demo.samples.bus;

import com.demo.model.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.util.Calendar;

/**
 * @author Aquanox
 * @date 30.09.2017 16:22
 */
@Component
public class SampleBusSender
{
    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendHello() throws InterruptedException
    {

        while (true)
        {
            jmsTemplate.convertAndSend(
                "mailbox",
                new Email("info@example.com",
                          "Hello from server (" + System.nanoTime() + ")",
                          Calendar.getInstance().getTime()
                )
            );
            Thread.sleep(1000);
        }
    }
}
