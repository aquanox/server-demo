package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Aquanox
 * @date 20.09.2017 13:59
 */
@SpringBootApplication
@EnableDiscoveryClient
public class DemoServiceB extends AbstractService
{
    public static void main(String[] args)
    {
        runService(SpringApplication.run(DemoServiceB.class, args));
    }
}

