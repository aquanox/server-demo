package com.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * @author Aquanox
 * @date 20.09.2017 14:35
 */
@RestController
@Slf4j
public class ServiceRestController
{
    private static final Random rand = new Random();

    @RequestMapping(value = "/welcome")
    public String welcome()
    {
        log.info("Access /welcome");

        List<String> greetings = Arrays.asList("Hi", "Welcome", "Sup");
        int randomNum = rand.nextInt(greetings.size());
        return greetings.get(randomNum);
    }

    @RequestMapping(value = "/")
    public String home() {
        log.info("Access /");
        return "Welcome!";
    }
}
