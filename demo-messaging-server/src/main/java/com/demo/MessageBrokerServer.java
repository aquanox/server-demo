package com.demo;

import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.console.CommandContext;
import org.apache.activemq.console.command.ShellCommand;
import org.apache.activemq.console.formatter.CommandShellOutputFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Aquanox
 * @date 24.09.2017 19:18
 */
@Slf4j
public class MessageBrokerServer
{
    public static void main(String[] args)
    {
        log.info("starting broker...");

        CommandContext context = new CommandContext();
        context.setFormatter(new CommandShellOutputFormatter(System.out));

        // Convert arguments to list for easier management
        List<String> tokens = new ArrayList<String>(Arrays.asList(args));
        customize(tokens);

        ShellCommand main = new ShellCommand();
        try
        {
            main.setCommandContext(context);
            main.execute(tokens);
        }
        catch (Exception e)
        {
            context.printException(e);
        }

    }

    private static void customize(List<String> tokens)
    {
        tokens.add("start");
    }

}
