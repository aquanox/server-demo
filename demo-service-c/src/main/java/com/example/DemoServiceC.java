package com.example;

import com.demo.model.Email;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

/**
 * @author Aquanox
 * @date 20.09.2017 13:59
 */
@SpringBootApplication
@EnableJms
public class DemoServiceC extends AbstractService
{
    @Bean // Serialize message content to json using TextMessage
    public MessageConverter jacksonJmsMessageConverter()
    {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

    public static void main(String[] args) throws InterruptedException
    {
        runService(DemoServiceC.class, args);

        while(true)
        {
            JmsTemplate jmsTemplate = getApplicationContext().getBean(JmsTemplate.class);

            // Send a message with a POJO - the template reuse the message converter
            System.out.println("Sending an email message.");
            jmsTemplate.convertAndSend("mailbox", new Email("info@example.com", "Hello"));

            Thread.sleep(5000);
        }

    }
}

