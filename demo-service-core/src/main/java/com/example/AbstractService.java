package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;

/**
 * @author Aquanox
 * @date 21.09.2017 00:45
 */
@Configuration
public abstract class AbstractService
{
    private static ConfigurableApplicationContext applicationContext;

    public static ConfigurableApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static void runService(Class<?> app, String[] args) {
        applicationContext = SpringApplication.run(app, args);
    }

    public static void runService(ConfigurableApplicationContext context) {
        applicationContext = context;
    }
}
